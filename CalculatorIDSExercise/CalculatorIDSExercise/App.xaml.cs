﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CalculatorIDSExercise
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            MainPage = new MainPage(new Helpers.CalculationHelper());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
