﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CalculatorIDSExercise.Helpers
{
    public class CalculationHelper
    {
        public string Calculate(double value1, string mathOperator, double value2)
        {
            double result = 0;

            switch (mathOperator)
            {
                case "+":
                    result = value1 + value2;
                    break;
                case "-":
                    result = value1 - value2;
                    break;
                case "×":
                    result = value1 * value2;
                    break;
                case "÷":
                    if (value2 == 0)
                    {
                        return "undefined";
                    }
                    else
                    {
                        result = value1 / value2;
                    }
                    break;
            }

            return result.ToString();
        }

        //Validations
        public bool IsStringNumberValid(string stringNumber)
        {
            double number;
            return double.TryParse(stringNumber, out number);
        }
    }
}
