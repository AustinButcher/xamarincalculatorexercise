﻿using System;
using Xamarin.Forms;
using CalculatorIDSExercise.Helpers;

namespace CalculatorIDSExercise
{
	public partial class MainPage : ContentPage
	{
		//Numbers are being stored as strings to prevent the loss of trailing zeros after a decimal point
		string firstNumber, secondNumber = "0";
		enum StateEnum
		{
			EnteringFirstNumber, EnteringSecondNumber
		}
		StateEnum state = StateEnum.EnteringFirstNumber;
		string mathOperator;
		CalculationHelper _calculationHelper;


		public MainPage(CalculationHelper calculationHelper)
		{
			InitializeComponent();
			_calculationHelper = calculationHelper;
		}

		void OnNumberClick(object sender, EventArgs e)
		{
			Button button = (Button)sender;
			string clickedValue = button.Text;

			//Validate input
			if (!IsNumberClickValid(clickedValue, resultText.Text))
            {
				return;
            }

			//Check for leading zero to remove
			resultText.Text = (resultText.Text == "0" && clickedValue != ".") ? resultText.Text = clickedValue : resultText.Text += clickedValue;

			if (state == StateEnum.EnteringFirstNumber)
			{
				firstNumber =  resultText.Text;
			}
			else if (state == StateEnum.EnteringSecondNumber)
			{
				secondNumber = resultText.Text;
			}
		}

		void OnOperationClick(object sender, EventArgs e)
		{
			Button button = (Button)sender;
			string clickedValue = button.Text;
			state = StateEnum.EnteringSecondNumber;
			mathOperator = clickedValue;
			resultTextPreviousLine.Text = $"{firstNumber} {clickedValue}";
			resultText.Text = "0";
		}

		void OnCalculateClick(object sender, EventArgs e)
		{
			if (state == StateEnum.EnteringSecondNumber)
			{
				//Calculate Result
				double firstNumberParsed, secondNumberParsed;
				double.TryParse(firstNumber, out firstNumberParsed);
				double.TryParse(secondNumber, out secondNumberParsed);
				var result = _calculationHelper.Calculate(firstNumberParsed, mathOperator, secondNumberParsed);

				//Ensure number will fit on the screen
				resultText.FontSize = (result.Length > 9) ? 24 : 72;
				resultText.Text = result.ToString();
				resultTextPreviousLine.Text = $"{firstNumber} {mathOperator} {secondNumber} =";

				//Reset variables
				firstNumber = result.ToString();
				secondNumber = "0";
				state = StateEnum.EnteringFirstNumber;
			}
		}

		void OnClearClick(object sender, EventArgs e)
		{
			//Reset everything
			resultText.FontSize = 72;
			firstNumber = "0";
			secondNumber = "0";
			state = StateEnum.EnteringFirstNumber;
			resultText.Text = "0";
			resultTextPreviousLine.Text = "";
		}

		void OnSignChangeClick(object sender, EventArgs e)
        {
			if (_calculationHelper.IsStringNumberValid(resultText.Text))
            {
				resultText.Text = (resultText.Text[0] == '-') ? resultText.Text.Remove(0, 1) : $"-{resultText.Text}";

				if (state == StateEnum.EnteringFirstNumber)
				{
					firstNumber =  resultText.Text;
				}
				else if (state == StateEnum.EnteringSecondNumber)
				{
					secondNumber = resultText.Text;
				}
            }
		}

		bool IsNumberClickValid(string clickedValue, string currentNumber)
		{
			//Don't allow the user to enter multiple decimal points.
			if (currentNumber.Contains(".") && clickedValue == ".")
			{
				return false;
			}
			//Ensure value is a valid number
			else if (!_calculationHelper.IsStringNumberValid(resultText.Text + clickedValue))
			{
				return false;
			}

			//Passed validation
			return true;
		}
	}
}
